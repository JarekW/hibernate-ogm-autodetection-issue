package com.example;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("")
@RequestScoped
public class ExampleEndpoint {

    @Inject
    private ArticleDao articleDao;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Article> index() {
        return articleDao.getArticles();
    }

    @GET
    @Path("/add")
    public String getArticle() {
        Article article = new Article("title");
        articleDao.saveArticle(article);
        return article.getId();
    }
}
