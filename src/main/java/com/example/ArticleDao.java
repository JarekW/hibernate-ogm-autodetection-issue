package com.example;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class ArticleDao {

    @PersistenceContext
    private EntityManager manager;

    public List<Article> getArticles() {
        String query = "db.Article.find({})";
        Query typedQuery = manager.createNativeQuery(query, Article.class);
        List<Article> resultList = typedQuery.getResultList();

        return resultList;
    }

    public void saveArticle(Article article) {
        manager.persist(article);
    }
}
