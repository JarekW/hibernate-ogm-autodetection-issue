package com.example;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;

@Entity
public class Article implements Serializable {

    @Id
    private String id;

    private String title;

    private Article() {
    }

    public Article(String title) {
        id = UUID.randomUUID().toString();
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
